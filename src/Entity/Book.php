<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Loan", mappedBy="books")
     */
    private $book_loan;

    public function __construct()
    {
        $this->book_loan = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Loan[]
     */
    public function getBookLoan(): Collection
    {
        return $this->book_loan;
    }

    public function addBookLoan(Loan $bookLoan): self
    {
        if (!$this->book_loan->contains($bookLoan)) {
            $this->book_loan[] = $bookLoan;
            $bookLoan->setBooks($this);
        }

        return $this;
    }

    public function removeBookLoan(Loan $bookLoan): self
    {
        if ($this->book_loan->contains($bookLoan)) {
            $this->book_loan->removeElement($bookLoan);
            // set the owning side to null (unless already changed)
            if ($bookLoan->getBooks() === $this) {
                $bookLoan->setBooks(null);
            }
        }

        return $this;
    }
}
