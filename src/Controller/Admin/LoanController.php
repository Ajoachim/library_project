<?php

namespace App\Controller\Admin;

use App\Entity\Loan;
use App\Form\LoanType;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LoanController
 * @package App\Controller\Admin
 * @Route("loan")
 */
class LoanController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function show(int $id, LoanRepository $repository)
    {
        $loans = $repository->findBy(
            [],
            ['name' => 'ASC']
        );

        return $this->render('admin/loan/index.html.twig',
            [
                'loans' => $loans
            ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     * @Route("/add_loan/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function newLoan(Request $request, EntityManagerInterface $em, $id)
    {
        if (is_null($id)){
            $loan = new Loan();
            $loan->setUsers($this->getUser());
        }else{
            $loan = $em->find(Loan::class, $id);

            if (is_null($loan)){
                throw new NotFoundHttpException();
            }
        }

        $form = $this->createForm(LoanType::class, $loan);

        $form->handleRequest($request);



        if ($form->isSubmitted()){
            if ($form->isValid()){
            $em->persist($loan);
            $em->flush();

            $this->addFlash('success', 'Vous avez réalisé un emprunt');
            }
        }

    }


}
