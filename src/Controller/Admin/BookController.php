<?php

namespace App\Controller\Admin;

use App\Entity\Book;
use App\Form\BookType;
use App\Form\SearchBookType;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BookController
 * @package App\Controller\Admin
 * @Route("/book")
 */
class BookController extends AbstractController
{

    /**
     * @Route("/")
     */
    public function index(Request $request, BookRepository $bookRepository)
    {
        $searchForm = $this->createForm(SearchBookType::class);
        $searchForm->handleRequest($request);

        dump($searchForm->getData());

        $books = $bookRepository->search((array)$searchForm->getData());

        return $this->render('admin/book/index.html',
            [
                'books' => $books,
                'search_form' => $searchForm->createView()
            ]);
    }
    /**
     * @Route("/show")
     */


    public function show(int $id, BookRepository $repository)
    {
        $books = $repository->findBy(
            [],
            ['name' => 'ASC']
        );

        return $this->render('admin/book/index.html.twig',
            [
            'books' => $books
        ]);
    }

    /**
     *
     *
     * @Route("add/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function addBook(Request $request, EntityManagerInterface $em, $id)
    {

        if (is_null($id)) {
            $book = new Book();
            $book->getBookLoan();
        } else {
            $book = $em->find(Book::class, $id);


            if (is_null($book)) {
                throw new NotFoundHttpException();
            }
        }
        $form = $this->createForm(BookType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if ($form->isValid()){
                $em->persist($book);
                $em->flush();

                $this->addFlash('success', 'Le livre a été ajouté');

                return $this->redirectToRoute('app_admin_book_index');

            }else{
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render('admin/book/add.html.twig',
            [
                'form' => $form->createView()
            ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("suppression/{id}")
     */
    public function delete(EntityManagerInterface $em, Book $book)
    {

        $title = $book->getTitle();
        $em->remove($book);
        $em->flush();

        $this->addFlash('success', "Le livre $title est supprimé ");

        return $this->redirectToRoute('app_admin_book_index');
    }


}
