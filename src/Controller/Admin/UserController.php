<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller\Admin
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function show(int $id, UserRepository $repository)
    {
        $users = $repository->findBy(
            [],
            ['name' => 'ASC']
        );

        return $this->render('admin/user/index.html.twig',
            [
                'users' => $users
            ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/add_user/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function addUser(Request $request, EntityManagerInterface $em, $id)
    {

        if (is_null($id)) {
            $user = new User();
        } else {
            $user = $em->find(User::class, $id);


            if (is_null($user)) {
                throw new NotFoundHttpException();
            }
        }
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if ($form->isValid()){
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Le livre a été ajouté');

                return $this->redirectToRoute('app_admin_user_show');

            }else{
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render('admin/user/add.html.twig',
            [
                'form' => $form->createView()
            ]);
    }

    /**
     * @param EntityManagerInterface $em
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("suppression_user/{id}")
     */
    public function delete(EntityManagerInterface $em, User $user)
    {

        $name = $user->getLastname();
        $em->remove($user);
        $em->flush();

        $this->addFlash('success', "L'abonné $name a été supprimé ");

        return $this->redirectToRoute('app_admin_user_show');
    }

}
