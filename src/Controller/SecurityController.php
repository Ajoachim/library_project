<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
       $user = new User();
       $form = $this->createForm(UserType::class, $user);
       $form->handleRequest($request);

       if ($form->isSubmitted()){
           if ($form->isValid()){
               $password = $passwordEncoder->encodePassword(
                   $user,
                   $user->getPlainPassword()
               );

               $user->setPassword($password);

               $em->persist($user);
               $em->flush();

               $this->addFlash('success', 'Votre compte est crée' );

               return $this->redirectToRoute('/');
           }else{
               $this->addFlash(
                   'error',
                   'Le formulaire contient des erreurs'
               );
           }
       }

       return $this->render('security/register.html.twig',
           [
               'form' => $form->createView()
           ]);

    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/connexion")
     */

    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if (!empty($error)){
            $this->addFlash('error', 'Identifiants incorrects');
        }

        return $this->render('security/login.html.twig',
            [
                'last_username' => $lastUsername
            ]);
    }

    /**
     * @Route("/deconnexion")
     */
    public function logout()
    {

    }
}
