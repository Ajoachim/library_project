<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class BookFixtures extends Fixture
{
    const COUNT = 50;
    public function load(ObjectManager $manager)
    {$faker = Factory::create('fr_FR');
    for ($i = 0; $i < self::COUNT; $i++){
        $book = new Book();
        $book->setTitle($faker->sentence);
        $book->setDescription($faker->text);
        $book->setAuthor($faker->userName);
        $manager->persist($book);
        $this->addReference('book'.$i, $book);
    }



        $manager->flush();
    }
}
